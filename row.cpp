#include "row.h"
#include "matrix.h"
#include "errors.h"
using namespace std;

#define TAB_POS_NEXT 1
#define NO_TAB_POS -1

Row::Row(int n) {
	for(int i = 0; i < n; i++) {
		elements.push_back(0);
	}
};

Row::Row(vector<double> el) {
	for(int i = 0; i < el.size(); i++) {
		elements.push_back(el[i]);
	}
};

Row& Row::operator=(const Row& r2) {
	if(r2.elements.size() != elements.size())
		throw RowLengthException();
	elements = r2.elements;
	return *this;
};

Row& Row::operator =(vector<double> el) {
	if(el.size() != elements.size())
		throw RowLengthException();
	elements = el;
	return *this;
};

Row Row::operator+(const Row& r2) const {
	if(r2.elements.size() != elements.size())
		throw RowLengthException();
	vector<double> temp;
	for(int i = 0; i < elements.size(); i++)
		temp.push_back(elements[i] + r2.elements[i]);
	return Row(temp);
};

Row& Row::operator+=(const Row& r2) {
	if(r2.elements.size() != elements.size())
		throw RowLengthException();
	for(int i = 0; i < elements.size(); i++)
		elements[i] += r2.elements[i];
	return *this;
};

Row Row::operator*(const Matrix& m) const {
	if(elements.size() != (m.get_matrix()).size())
		throw MatrixMultException();
	vector <Row> temp1;
	temp1.push_back(*this);
	Matrix temp(temp1);
	Matrix result = temp * m;
	return (result.get_matrix())[0];
};

Row operator+(const double d, const Row& r) {
	vector<double> temp;
	for(int i = 0; i < (r.get_elements()).size(); i++)
		temp.push_back((r.get_elements())[i] * d);
	return Row(temp);
};

double& Row::operator[](int i) {
	if(i < 0 || i > elements.size())
		throw IndexOutOfBoundException();
	return elements[i];
};

const double& Row::operator[](int i) const {
	if(i < 0 || i > elements.size())
		throw IndexOutOfBoundException();
	return elements[i];
};

bool Row::operator==(const Row& r2) const {
	bool is_equal = true;
	if(elements.size() != (r2.get_elements()).size())
		return !is_equal;
	for(int i = 0; i < elements.size(); i++)
		if(elements[i] != (r2.get_elements())[i])
			is_equal = false;
	return is_equal;
};

ostream& operator<<(ostream& out, const Row& r) {
    for(int i = 0; i < r.elements.size(); i++) {
    	if(i < r.elements.size() - 1)
    		out << r.elements[i] << '\t';
    	else
    		out << r.elements[i];
    }
    return out;
};

istream& operator>>(istream& in, Row& r) { 
    string s;
	getline(in, s, '\n');
	vector<double> temp = tab_parser(s);
	r.elements.clear();
	for(int i = 0; i < temp.size(); i++)
		r.elements.push_back(temp[i]);
    return in;          
};

vector<double> tab_parser(string s) {
	vector<double> temp;
	while(s.length()>0) {
		int divider_pos = s.find('\t');
		temp.push_back(atof(s.substr(0, divider_pos).c_str()));
   		s = s.substr(divider_pos + TAB_POS_NEXT);
		if(divider_pos == NO_TAB_POS)
			break;
	}
	return temp;
};