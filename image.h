#include <iostream>
#include <vector>
#include <cstdlib>
#include <string>
#include "bitmap_image.h"
#include "matrix.h"
#include "errors.h"

typedef std::vector<std::vector<int> > ColorMap;

class Image {
public:
	Image();
	Image(std::string file_name);
	Image& operator=(const Image& i2);
	Image operator!();
	Image operator*(Matrix m);
	Image grayscale();
	
	int height() { return image.height(); };
	int width() { return image.width(); };

	void save(std::string file_name);
private:
	bitmap_image image;
	ColorMap r, g, b;
};

void bmp_to_vector(const bitmap_image& image, ColorMap& r, ColorMap& g, ColorMap& b);
int r_convolved(int i, int j, const ColorMap& r, const std::vector<Row>& m);
int g_convolved(int i, int j, const ColorMap& g, const std::vector<Row>& m);
int b_convolved(int i, int j, const ColorMap& b, const std::vector<Row>& m);