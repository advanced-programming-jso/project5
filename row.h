#include <iostream>
#include <vector>
#include <cstdlib>
#include <string>

class Matrix;
class Image;

#ifndef ROW_H
#define ROW_H
	class Row {
	public:
		Row(int n);
		Row(std::vector<double> el);

		Row& operator=(const Row& r2);
		Row& operator=(std::vector<double> el);
		Row operator+(const Row& r2) const;
		Row& operator+=(const Row& r2);
		Row operator*(const Matrix& m) const;
		Row operator*(double d) const;
		double& operator[](int i);
		const double& operator[](int i) const;
		bool operator==(const Row& r2) const;
		
		int size(){ return elements.size(); };
		//int size() const{ return elements.size(); };

		std::vector<double> get_elements() const { return elements; };

		friend std::istream& operator>>(std::istream& in, Row& r);
		friend std::ostream& operator<<(std::ostream& out, const Row& r);
	private:
		std::vector<double> elements;
	};

	Row operator+(const double d, const Row& r);
	std::vector<double> tab_parser(std::string s);
#endif 