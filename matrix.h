#include "row.h"

#ifndef MATRIX_H
#define MATRIX_H
	class Matrix {
	public:
		Matrix();
		Matrix(int r, int c);
		Matrix(std::vector<Row> v);
		Matrix(double n);
		Matrix& operator=(const Matrix& m2);
		Matrix& operator=(const Row& r);
		Matrix operator+(const Matrix& m2) const;
		Matrix operator*(Matrix m2);
		Matrix& operator*=(Matrix m2);
		Matrix covolve(Matrix m2);
		int elements_sum();

		int height() { return matrix.size(); };
		int width() { return matrix[0].size(); };
		//int height() const{ return matrix.size(); };
		//int width() const{ return matrix[0].size(); };

		void insert(Row r, int i);
		std::vector<Row> get_matrix() const { return matrix; };

		friend std::istream& operator>>(std::istream& in, Matrix& m);
		friend std::ostream& operator<<(std::ostream& out, const Matrix& m);
	private:
		std::vector<Row> matrix;
	};

	std::vector<std::string> row_parser(std::string s);
#endif 