#declare the variable
CC=g++

CFLAGS=-c

all: core

core: errors.o image.o row.o matrix.o main.o
	  	$(CC) errors.o image.o row.o matrix.o main.o -o output

main.o: main.cpp
		$(CC) $(CFLAGS) main.cpp

matrix.o: matrix.cpp matrix.h
		$(CC) $(CFLAGS) matrix.cpp

row.o: row.cpp row.h
		$(CC) $(CFLAGS) row.cpp

image.o: image.cpp image.h bitmap_image.h
		$(CC) $(CFLAGS) image.cpp

errors.o: errors.cpp errors.h 
		$(CC) $(CFLAGS) errors.cpp

clean:
		rm -rf *o core