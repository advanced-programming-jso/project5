#include "matrix.h"
#include "errors.h"
using namespace std;

#define ENTER_POS_NEXT 1
#define NO_ENTER_POS -1

Matrix::Matrix() {

};

Matrix::Matrix(int r, int c) {
	if(r > 0 && c > 0)
		for(int i = 0; i < r; i++) 
			matrix.push_back(Row(c));
};

Matrix::Matrix(vector<Row> v) {
	if(v.size() > 0 && v[0].size() > 0)
		for(int i = 0; i < v.size(); i++)
			matrix.push_back(v[i]);
};

Matrix::Matrix(double n) {
	vector<double> temp;
	temp.push_back(n);
	matrix.push_back(Row(temp));
};

Matrix& Matrix::operator=(const Matrix& m2) {
	matrix.clear();
	for(int i = 0; i < m2.matrix.size(); i++)
		matrix.push_back(m2.matrix[i]);
	return *this;
};

Matrix& Matrix::operator=(const Row& r) {
	matrix.clear();
	matrix.push_back(r);
	return *this;
};

Matrix Matrix::operator+(const Matrix& m2) const {
	if(matrix.size() != m2.matrix.size())
		throw MatrixLengthException();
	vector<Row> temp;
	for(int i = 0; i < matrix.size(); i++)
		temp.push_back(matrix[i] + m2.matrix[i]);
	return Matrix(temp);
};

Matrix Matrix::operator*(Matrix m2) {
	if(matrix[0].size() != m2.matrix.size())
		throw MatrixMultException();
	vector<double> temp1;
	vector<Row> temp2;
	for(int i = 0; i < matrix.size(); i++) {
		temp1.clear();
		for(int j = 0; j < m2.matrix[0].size(); j++) {
			double sum = 0;
			for(int k = 0; k < m2.matrix.size(); k++) {
				sum += matrix[i][k] + m2.matrix[k][j];
			} 
			temp1.push_back(sum);
		}
		temp2.push_back(temp1);
	}
	return Matrix(temp2);
};

Matrix& Matrix::operator*=(Matrix m2) {
	if(matrix[0].size() != m2.matrix.size())
		throw MatrixMultException();
	vector<double> temp1;
	vector<Row> temp2;
	for(int i = 0; i < matrix.size(); i++) {
		temp1.clear();
		for(int j = 0; j < m2.matrix[0].size(); j++) {
			double sum = 0;
			for(int k = 0; k < m2.matrix.size(); k++) {
				sum += matrix[i][k] + m2.matrix[k][j];
			} 
			temp1.push_back(sum);
		}
		temp2.push_back(temp1);
	}
	matrix.clear();
	matrix = temp2;
	return *this;
};

Matrix Matrix::covolve(Matrix m2) {
	Matrix temp = m2;
	for(int i = 0; i < m2.height(); i++){
		for(int j = 0; j < m2.width(); j++){
			temp.matrix[i][j] = matrix[i][j] * m2.matrix[i][j];
		}
	}
	return temp;
};

void Matrix::insert(Row r, int i) {
	matrix.insert(matrix.begin() + i, r);
};

int Matrix::elements_sum() {
	int sum = 0;
	for(int i = 0; i < (*this).height(); i++){
		for(int j = 0; j < (*this).width(); j++){
			sum += matrix[i][j];
		}
	}
	return sum;
};

ostream& operator<<(ostream& out, const Matrix& m) {
    out << '[' << endl;
    for(int i = 0; i < m.matrix.size(); i++) {
    	out << m.matrix[i] << endl;
    }
    out << ']';
    return out;
};

istream& operator>>(istream& in, Matrix& m) { 
	string s;
	in >> s;
	getline(in,s,']');
	vector<string> temp_string = row_parser(s);
	m.matrix.clear();

	vector<double> temp;
	for(int i = 0; i < temp_string.size(); i++) {	
		double dob;
		temp = tab_parser(temp_string[i]);
		if(i > 0)
			m.matrix.push_back(Row(temp));
		temp.clear();
	}
	return in; 
};

vector<string> row_parser(string s) {
	vector<string> temp;
	while(s.length()>0) {
		int enter_pos = s.find('\n');
		temp.push_back(s.substr(0, enter_pos));
   		s = s.substr(enter_pos + ENTER_POS_NEXT);
		if(enter_pos == NO_ENTER_POS)
			break;
	}
	return temp;
};