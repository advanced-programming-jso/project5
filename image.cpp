#include "image.h"
#include <cmath>
using namespace std;

Image::Image() {

};

Image::Image(string file_name) {
	bitmap_image image2(file_name);
   	if (!image2) {
      	printf("- Error - Failed to open '%s'\n",file_name.c_str());
      	return;
   	}
   	image = image2;
   	bmp_to_vector(image, r, g, b);
};

void bmp_to_vector(const bitmap_image& image, ColorMap& r, ColorMap& g, ColorMap& b) {
	unsigned char red, green, blue;
	for(int i = 0; i < image.width(); i++) {
		vector<int > r1, g1, b1;
		for(int j = 0; j < image.height(); j++) {
			image.get_pixel(i, j, red, green, blue);
			r1.push_back((int)red);
			g1.push_back((int)green);
			b1.push_back((int)blue);
		}
		r.push_back(r1);
		g.push_back(g1);
		b.push_back(b1);
	}
};

Image& Image::operator=(const Image& i2) {
	image = i2.image;
	bmp_to_vector(image, r, g, b);
	return *this;
};

Image Image::operator!() {
	Image temp;
	temp.image = image;
	for(int i = 0; i < r.size(); i++) {
		for(int j = 0; j < r[0].size(); j++) {
			temp.image.set_pixel(i, j, 256 - r[i][j], 256 - g[i][j], 256 - b[i][j]);
		}
	}
	return temp;
};

Image Image::operator*(Matrix m) {
	if(m.width() != m.height() && m.width() % 2 == 0)
		throw ConvolveException();
	Image temp;
	temp.image = image;
	vector<Row> matrix = m.get_matrix();
	int r_temp, g_temp, b_temp;
	for(int i = 1; i < r.size() - 1; i++) {
		for(int j = 1; j < r[0].size() - 1; j++) {
			r_temp = r_convolved(i, j, r, matrix);
			g_temp = g_convolved(i, j, g, matrix);
			b_temp = b_convolved(i, j, b, matrix);
			//cout << "( " << i << ", " << j << ")  " << r_temp << ":::" << g_temp << ":::" << b_temp << endl;
			temp.image.set_pixel(i, j, r_temp, g_temp, b_temp);
		}
	}
	return temp;
};

int r_convolved(int i, int j, const ColorMap& r, const vector<Row>& m) {
	
	int r_temp = r[i-1][j-1] * m[0][0] + 
			r[i-1][j] * m[0][1] +
			r[i-1][j+1] * m[0][2] +
			r[i][j-1] * m[1][0] +
			r[i][j] * m[1][1] +
			r[i][j+1] * m[1][2] +
			r[i+1][j-1] * m[2][0] +
			r[i+1][j] * m[2][1] +
			r[i+1][j+1] * m[2][2];
	if(r_temp < 0)
		r_temp = 0;
	if(r_temp > 255)
		r_temp = 255;
	return r_temp;
};

int g_convolved(int i, int j, const ColorMap& g, const vector<Row>& m) {
	int g_temp = g[i-1][j-1] * m[0][0] + 
			g[i-1][j] * m[0][1] +
			g[i-1][j+1] * m[0][2] +
			g[i][j-1] * m[1][0] +
			g[i][j] * m[1][1] +
			g[i][j+1] * m[1][2] +
			g[i+1][j-1] * m[2][0] +
			g[i+1][j] * m[2][1] +
			g[i+1][j+1] * m[2][2];
	if(g_temp < 0)
		g_temp = 0;
	if(g_temp > 255)
		g_temp = 255;
	return g_temp;
};

int b_convolved(int i, int j, const ColorMap& b, const vector<Row>& m) {
	int b_temp = b[i-1][j-1] * m[0][0] + 
			b[i-1][j] * m[0][1] +
			b[i-1][j+1] * m[0][2] +
			b[i][j-1] * m[1][0] +
			b[i][j] * m[1][1] +
			b[i][j+1] * m[1][2] +
			b[i+1][j-1] * m[2][0] +
			b[i+1][j] * m[2][1] +
			b[i+1][j+1] * m[2][2];
	if(b_temp < 0)
		b_temp = 0;
	if(b_temp > 255)
		b_temp = 255;
	return b_temp;
};

Image Image::grayscale() {
	Image temp;
	temp.image = image;
	int r_temp, g_temp, b_temp;
	for(int i = 0; i < r.size(); i++) {
		for(int j = 0; j < r[0].size(); j++) {
			r_temp = g_temp = b_temp = (r[i][j] + g[i][j] + b[i][j]) / 3;
			temp.image.set_pixel(i, j, r_temp, g_temp, b_temp);
		}
	}
	return temp;
};

void Image::save(string file_name) {
	image.save_image(file_name);
};